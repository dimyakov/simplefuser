# КОТоС (Комплекс Обработки Томографических Снимков)

Десктопное приложение для просмотра томографических медицинских изображений, направленное на совместный анализ и слияние снимков.

Некоторые сложные исследования из области лучевой диагностики требуют совместного анализа снимков, полученных с помощью разных физических принципов (например, КТ и МРТ). В таком случае для повышения качества анализа часто применяется методика наложения снимков разной природы. Уникальность данного проекта состоит в применении технологий искусственного интеллекта для решения задачи наложения изображений в области медицинских исследований.

## Обзор интерфейса
Приведём пример работы приложения. При запуске виден стартовый экран.

![](assets/images/empty-screen.jpg)
*Интерфейс приложения*

С правой стороны экрана расположена панель функциональных кнопок. Там находятся кнопки загрузки снимков и слияния изображений. Также там должна быть кнопка сохранения с снимка, однако такая функция ещё не была реализована.

На экране присутствуют две одинаковые панели просмотра снимков. Каждая такая панель включает в себя окно просмотра и необходимые управляющие элементы.

![](assets/images/viewer-review.jpg)\
*Панель просмотра снимков*

Над окном просмотра изображения (1) расположен бегунок выбора снимка из серии (2). Каждое рентгенологическое исследование представляет собой массив срезов в той или иной проекции. Данный бегунок нужен для того, чтобы выбрать из массива нужный снимок. Справа от окна просмотра – шкала увеличения (3). Она позволяет увеличивать просматриваемое изображение. Для масштабирования можно перетаскивать бегунок или кликать по числам справа. Кнопка в правом верхнем углу панели – кнопка центрирования (4). Она позволяет центрировать изображение. Под окном просмотра расположен выпадающий список выбора исследования (6). Он позволяет переключаться между загруженными исследованиями. Под ним расположены элементы регулировки яркости (6) и контрастности (7). В каждом из них значения могут варьироваться от 0 до 200 процентов. Далее идёт блок раскраски. Ключевым элементом здесь является выключатель раскраски (8). Раскраска применяется к изображению только когда он включён. Изображение раскрашивается в зависимости от яркости пикселя оригинала. Если пиксель имеет яркость, попадающую в интервал между нижней и верхней границей, он будет раскрашен в красный цвет. За изменение границ отвечают соответствующие элементы управления: (9) и (10).


Для загрузки изображения нужно нажать соответствующую кнопку и выбрать DICOM-папку в файловом диалоге. После завершения загрузки серии снимков появится окно масштабирования, позволяющее изменить размер снимков.

![](assets/images/resize-dialog.jpg)\
*Диалоговое окно масштабирования*

## Регистрация изображений
Из загруженных серий выберем два снимка:

![](assets/images/ct.jpg) ![](assets/images/mri.jpg)\
*Загруженные КТ и МРТ снимки*

Далее их нужно сопоставить друг с другом, провести регистрацию изображений. Наглядный пример необходимости регистрации приведён на рисунке ниже.

![](assets/images/registration-example.jpg)\
*Пример использования регистрации изображений: КТ снимок (сверху слева), МРТ снимок (сверху справа), наложенные КТ и МРТ без регистрации (снизу слева), наложенные КТ и МРТ с регистрацией (снизу справа)*

Для проведения регистрации нужно на каждом снимке пары расставить точки (с помощью ПКМ), который должны совпадать друг с другом на результирующем изображении. 

![](assets/images/ct%20with%20points.jpg) ![](assets/images/mri%20with%20points.jpg)\
*Примерная расстановка точек регистрации*

Существуют разные подходы к регистрации изображений. Самый простой из них: ручной поворот, масштабирование, перенос – как в каком-нибудь графиеском редакторе. Однако, такой способ не слишком эффективен как по времени, так и по качеству результата. Наиболее эффективной является автоматическая регистрация, однако при решении данной задачи её применение затруднено из-за небоходимости обработки изображений разных модальностей и малым количество данных. Таким образом, был предложен промежуточный вариант: пользователь отмечает точки, которые должны совпадать, а алгоритм по ним вычисляет оптимальное преобразование.

Так как точки не упорядочены друг относительно друга, то применяется перебор всех возможных перестановок, дабы по ошибке полученного на каждой итерации преобразования определить, какой порядок является нужным.
Данный метод не является вычислительно эффективным, однако его можно значительно ускорить применением кластеризации точек и временной заменой целого кластера одной точкой – средним значением координат всех точек кластера.

Получаемое преобразование является преобразованием подобия и не деформирует изменяемое изображение.

## Слияние изображений
После проведения регистрации можно применять алгоритм попиксельного слияния изображения. В основе алгоритма интеллектуального наложения изображений лежит слияние на слое яркости. Изображение переводится из цветовой схемы BGR (Blue, Green, Red) в цветовую схему YCbCr (Яркость, Chroma blue, Chroma red) с последующим наложением изображений по каждому каналу (слою). Наложение каналов Cr и Cb проводится по специальной формуле. Данные канала яркости накладываются друг на друга с помощью свёрточной нейронной сети. Сеть принимает трёхмерный тензор яркостей пикселей, пропускает его через два слоя свёртки. Из выхода берётся среднее по всем «слоям» тензора и затем вновь пропускается по свёрточным слоям. На выходе получается матрица размером, равным размеру изображений. Для корректной работы алгоритма важно, чтобы в него подавались изображения одинакового размера. В качестве преимущества данного алгоритма можно выделить повышенную чёткость получаемых изображений.


Для слияния изображений нужно нажать соответствующую кнопку. Среди загруженных снимков появится результат.

![](assets/images/ct%2Bmri%20gray.jpg)\
*Результат слияния*

Для более понятной картинки, можно раскрасить часть КТ снимка.

![](assets/images/ct%2Bmri%20red.jpg)\
*Раскрашенный результат слияния*
